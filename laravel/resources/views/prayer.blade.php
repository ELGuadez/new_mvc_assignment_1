@extends('layout')


@section('content')
    
     <!--+++++++++++start of content+++++++++++++-->
    <section>
      <div id="pray_wrapper">
        <div id="prayer_main_box4">
         <img src="images/praygray90511.jpg" alt="Pray" height="400" width="905" >
         <div id="box4a">
           <h1>Need a Prayer? We can help you.</h1>
         </div>
         </div>
         
         <div id="prayer_box4a">
             <p id="prayer"><span>There are situations in each of our lives when we need others to support, encourage and pray for us. We have amazing people here at The Filipino Evangelical Church who would love to help.
            Please complete the form below to submit your prayer request</span><p>

         </div>
         
         <div id="prayer_pageform">
            <form  id="personal"
                   name="Only_form"
                   method="post"
                   action="http://www.scott-media.com/test/form_display.php"
                   autocomplete="off" >
                   
              <fieldset id="fieldset1">
               
                   <p>
                     <!--type text form element-->
                     <label for="first_name" class="label_info">Your Name</label>
                     <input type="text"
                            name="first_name"
                            id="first_name"
                            required
                            placeholder="Type your name"
                            tabindex="1"
                            class="textfield"/> 
                   </p>
                 
                   
                    <p>
                      <!--email-->
                      <label for="email_add" class="label_info">Email</label>
                      <input type="email" 
                             name="email_add"
                             id="email_add"
                             required
                             tabindex="2"
                             class="textfield"/>
                    </p>
                      <p>
                        <select name="language" id="language" tabindex="3"><!--option-->
                          <option value="language_selected">Select you preferred language</option>  
                            <option value="Language">English</option>
                            <option value="Language">French</option>
                            <option value="Language">Tagalog</option><!--option-->
                        </select>
                  </p>
                
                  <p>
                    <!--text area-->
                    <label for="request" class="label_info">Type your prayer request</label>
                      
                      <textarea name="request" 
                               id="request"
                               placeholder="Type your prayer request"
                               class="subject"
                               tabindex="4">
                      </textarea>

                    
                  </p>

                  <p>
                    <input type="submit" 
                           value="Submit" 
                           tabindex="5"
                           class="buttons"
                          /> &nbsp;

                    <input type="reset" 
                           value="Clear" 
                           class="buttons"
                            /> &nbsp;
                  </p>
                  <p>
                    <label for="subscribe" class="subscribe">Check to subscribe to our Newsletter</label><br />
                     <input type="checkbox"
                            id="subscribe"
                            name="Subscribe"
                            value="Subscribe to our newsletter"
                            tabindex="6"/>
                  </p>
              </fieldset>

            </form>

          </div><!--Page text-->
         
    </div><!--wrapper close-->
    </section>

@endsection
  
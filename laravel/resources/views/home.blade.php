@extends('layout')


@section('content')
    
    <section>
      <div id="wrapper">
       
        <div id="box1_mainpic" class="grid-box">
          <img src="images/holyspirit_1509black.jpg" id="sky" alt="FEC" />
        </div><!--main image-->
        
        <!--+++++++++1st row of boxes++++++++++++-->
        <div id="main_box2">
          <div id="box2_a">
            <h1>Filipino Evangelical Church</h1>

            <p class="fectext">
            is a Christ-centered, bible-based community of believers. 
            FEC’s purpose is to glorify God in words and deeds, 
            to serve Him in the power of the Holy Spirit; 
            leading unbelievers to faith in Jesus Christ
            and believers to maturity.</p>
          </div>

          <div id="box2_b">
            <h1>Bible verse for the day</h1>

            <h3>Proverbs 9:11-12</h3>
            <p>Wisdom will multiply your days and add years to your life.
            If you become wise, you will be the one to benefit. If you score
            wisdom, you will be the one to suffer.</p>
          </div>
          
          <div id="box2_c">
            <h1 id="add" style="font-weight: 700">Announcements:</h1>
            <h3>Youth band rehearsals</h3>
            <p>Adolescents who want join the FEC band will have 
            rehearsals on April 20, 2019, 2pm at FEC church</p>
            
            <h3>Women's fellowship</h3>
            <p>Members of the FEC womens group will have a fellowship this
            coming April 21, 2019, 4pm at FEC church</p>
          </div>  
       </div>
       
       <!--+++++++++++++ 2nd row of boxes++++++++++-->
       <div id="main_box3">
         <div id="box3_a">
           <img src="images/fecchurch373black.jpg" alt="FEC church">
         </div>
         
         <div id="box3_b">
           <img src="images/praise373.jpg" alt="FEC church">
         </div>
         
         <div id="box3_c">
           <img src="images/shekinah.jpg" alt="Praise">
         </div>
       </div>
       
      <!--+++++++++Image of praying hands on the last row+++++++++++-->
       <div id="main_box4">
         <img src="images/praygray90511.jpg" alt="Pray">
         <div id="box4a">
           <h1>Need a Prayer?</h1>
           <h2 id="nomatter">No matter what you're facing we love to pray with you</h2>
           <p id="prayer"><span><a href="/prayer" class="cool-link {{ ($slug == 'prayer')? 'active' : '' }}">&#128591;&#127996; Ask for Prayer</a></span><p>
           
         </div>
       </div>
        
      </div><!--wrapper close-->
    </section>

    @endsection
    
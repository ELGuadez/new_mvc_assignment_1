
@extends('layout')


@section('content')
    
    <section>
      <div id="wrapper_ministry">
        
        <h1>Ministries</h1>
        <h2>Men's Ministry</h2>
        <p>FEC is an active member of Winnipeg soceity. FEC Mens basketball team is actively participating 
        in inter-church basketball league in the city. Through fellowship with other
        churches is also actively volunteering to give back to the city of Winnipeg. 
        FEC is also sending participants to missionary services to other places
        in Canada, Mexico and the Philippines.</p>

        <h2>Women's</h2>
        <p>FEC Womens fellowship is and active member of Manitoba womens associations.
        Through the efforts of FEC women, funds were raise to assist in the financial
        support of mountain pastors in the Philippines. </p>

        <h2>Young Adult's Ministry</h2>
        <p>SERVE is a youth activity every year. FEC is sending youth participants to
        youth missions outside Winnipeg. SERVE is a weeklong missionary work, it is 
        a volunteer work helping other people and also involves preaching the word of God </p>

        <h2>Children's Ministry</h2>
        <p>FEC is very active in creating fun activities for young children. IN fact, FEC have 
        weekly activity called AWANA wherein the children will play parlor games,
        numbers games and bible studies. On Sundays, FEC is also conducting sunday schooling for
        kids, teaching them good values and encouraging them to participate in the community and
        help others in need. </p> 
        
      </div><!--wrapper close-->
    </section>


@endsection
    
    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
    <title>Layout</title>
    <link rel="stylesheet" 
          href="/styles/desktop.css"
          type="text/css"
          media="screen"
    />
    
   <link rel="stylesheet"
          href="/styles/mobile.css"
          type="text/css"
          media="screen"
    />
  
    <link rel="stylesheet"
          href="/styles/print.css"
          type="text/css"
          media="print"
    
    />
  
    <!-- Conditional comments for IE browsers -->
    
    <!--[if LTE IE 8]>
      <link rel="stylesheet" href="old_ie.css" type="text/css" />
    <![endif]-->
    
  </head>
  <body>
    
     <!--[if LTE IE 8]>
          <div>You are using a very old browser. Time to upgrade</div>
        <![endif]-->
        
  <!--   <!--+++++++++header styles+++++++++++-->
    <header>
      <!--++++++++logo++++++++++-->
      <div id="logo_150"> 
        <a href="index.html"><img src="/images/fec10041.jpg" alt="FEC Logo"/></a>
      </div>
      <div id="logo_100">
        <a href="index.html"><img src="/images/fec100x100.jpg" alt="FEC Logo"/></a>
      </div>
       
       <!--++++++++++navigation+++++++++++-->
      <nav>
        <ul id="navlist">
         <li><a href="/" class="cool-link {{ ($slug == 'home')? 'active' : '' }}">Home</a></li>
         <li><a href="/about" class="cool-link {{ ($slug == 'about')? 'active' : '' }}">About</a></li>
         <li><a href="/activities" class="cool-link {{ ($slug == 'activities')? 'active' : '' }}">Activities</a></li>
         <li><a href="/ministry" class="cool-link {{ ($slug == 'ministry')? 'active' : '' }}"><span>Ministry</span></a></li>
         <li><a href="/ecommerce" class="cool-link {{ ($slug == 'ecommerce')? 'active' : '' }}">Ecommerce</a></li>
         <li><a href="/address" class="cool-link {{ ($slug == 'address')? 'active' : '' }}">Location</a></li>
        </ul>
       </nav>
        
       <!--++++location and phone #+++++++++-->  
       <div id="location">
         <div class="address">
           <p>
           <a href="/address" class="cool-link {{ ($slug == 'address')? 'active' : '' }}">977 Spruce Street<br>Winnipeg, Manitoba R3G 3A1</a>
             <p class="services">Sunday Services: 11am - 1pm
           </p>
         </div> 
         <div class="tel">
           <p><a href="#">&#9743; (204)-775-5679</a></p>
         </div>
       </div>  
    </header>



      
 @yield('content')

    





    
    
     <!--++++++++++footer+++++++++++++-->
    <footer>
      <!--+++++++++social media icons++++++++++-->
      <div id="social_media">
        <ul id="fb">
          <li><a href="#"><img src="/images/fb.png" alt="facebook"/></a></li>
          <li><a href="#"><img src="/images/email.png" alt="email"/></a></li>
          <li><a href="#"><img src="/images/twitter.png" alt="twitter"/></a></li>
        </ul>
      </div>
      <div>
        <span class="copyright">Copyright &copy; 2019 by the Filipino Evangelical Church. All Rights Reserved</span>
      </div>
    </footer>
  </body>
</html>

@extends('layout')


@section('content')

<div id="ecommerce_details_image">
        <h1 class="ecommerce_details_name">{{$product['name']}}</h1>
        <img src="/images/fec_store/gallery/{{$product['image']}}" alt="product category">
        <br/>
        <a href="/ecommerce" ><p class="back">Back to previous page</p></a>
</div>
     @csrf
      
      <div id="ecommerce_details_info">
          <p ><strong>Category: </strong>{{$product['category']}}</p>
          <p ><strong>Price: </strong>{{$product['price']}} CAD</p>
          <p ><strong>Stocks Available: </strong>{{$product['stocks_available']}}</p>
          <input type="hidden" name="id"
                value="{{$product['id']}}" />
          
        
      </div> <!-- div info -->
      <div id="ecommerce_details_description">
        <p style="text-align: justify;">{{$product['description']}}></p>
      </div>
    

@endsection
@extends('layout')


@section('content')
    
<section>
    <div id="wrapper">

        <div id="main_box2">

            <!--++++++++++++++first row of boxes with text+++++++++++++++-->
            <div id="box2_a">
                <h1>Sunday Services</h1>

                <p class="fectext">
                    All are welcome to join our Sunday mass celebration at 977
                    Spruce st., Winnipeg City. Our service starts at 11am to 1pm.
                    Bring your friends and family we have places for kids too. Every
                    Sunday FEC have bible studies and games for children.</p>
            </div>

            <div id="box2_b">
                <h1>Children's Ministry</h1>

                <h3>AWANA</h3>
                <p>AWANA is an activity for children that is more on fun and
                    games. Learning about Jesus and good moral values while having fun.
                    AWANA is facilitated by active youth and adult FEC members. It is weekly activity
                    scheduled every Friday, 6pm. All are invited to bring their kids
                    so that they can meet new friends.</p>
            </div>

            <div id="box2_c">
                <h1>Family Camp</h1>

                <p>Every year FEC is planning for a big event usually outside Winnipeg.
                    It is the family camp, It is a celebration during the long weekend Labor day on the Month of
                    September. FEC families travel together to Camp Nutimik, at Whitehorse,
                    Manitoba. It is a time to chill, away from work and studies and just
                    bonding with friends. We hope you can join us this coming September
                </p>

            </div>
        </div>

        <!--++++++++Row 2 is boxes with a lot of challenging quality of pictures++++++-->
        <div id="row_2main_box3">
            <div id="row_2box3_a">
                <img src="images/simbahan.jpg" alt="FEC church">
            </div>

            <div id="row_2box3_b">
                <img src="images/awana1.jpg" alt="Awana games">
            </div>

            <div id="row_2box3_c">
                <img src="images/familycamp2.jpg" alt="family camp">
            </div>
        </div>
        <!--+++++++++++++++++Row 3 is also boxes of tasteful pictures+++++++++-->
        <div id="row_3main_box3">
            <div id="row_3box3_a">
                <img src="images/ss1.jpg" alt="sunday services">
            </div>

            <div id="row_3box3_b">
                <img src="images/awana2.jpg" alt="awana facilitators">
            </div>

            <div id="row_3box3_c">
                <img src="images/familycamp4.jpg" alt="family camping">
            </div>
        </div>

    </div>
    <!--wrapper close-->
</section>


@endsection
    
   
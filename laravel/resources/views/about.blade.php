@extends('layout')


@section('content')


    <section>
      <div id="about_wrapper">
  
        <div id="about_main">
         
          <div id="about_background">
            
            <h1>Background</h1>

            <p>The Filipino Evangelical Church or FEC is a religious organization based in west end Winnipeg. It was founded by the 
            first generation of Filipino immigrants to Manitoba in the early 70’s. At that time, its primary purpose is to reach out 
            spiritually and socially to Filipino immigrants to Winnipeg. In recent years however, its membership had expanded to other 
            ationalities living here in Winnipeg.</p>

            <p>FEC is locally supporting Bethlehem Aboriginal Fellowship and Missionfest Manitoba. Globally, FEC is supporting missionaries 
            in the Philippines and countries hit by calamities. Through the Canadian Baptist Ministries, FEC, provides monthly financial 
            support to 8 mountain pastors in the Philippines and the FEC Women’s Fellowship provides scholarships to pastoral students.
            In 2018, FEC celebrated its 35th year founding anniversary. </p>

          </div>
         
          <div id="about_mission">
            <h1>Mission and Vision</h1>

            <p>
            is a Christ-centered, bible-based community of believers.<br> 
            FEC’s purpose is to glorify God in words and deeds, 
            to serve <br>Him in the power of the Holy Spirit; 
            leading unbelievers<br> to faith in Jesus Christ
            and believers to maturity.</p>
          </div>
        </div>
          <h2>Ministers </h2>
          <div id="about_ministers">
            <div id="clinton">
              <img src="images/smiley.jpg" alt="Pastor Clinton"/>
              <p><span>Pastor Clinton Legaspi</span></p>
            </div>
            <div id="roslyn">
             <img src="images/smiley200.jpg" alt="Reverend Roslyn"/>
             <p><span>Reverend Roslyn Pamplona</span></p>
            </div>
          </div>
          
          <table><!--start, to create a table-->

            <caption></caption><!-- Caption above the table -->
            <!-- Title of the table  -->
            <tr>
              <th colspan="4">FEC Board</th><!--table header-->
            </tr>

            <tr class="board">
              <th colspan="2">Board of Deacons</th>
              <th colspan="2">Board of Trustees</th>
            </tr>


            <tr>
              <td>Deacon chair and mission evangelism</td>
              <td>Retolio "Toto" Sela</td>
              <td>BOT Chair/ Administration/Personnel</td>
              <td>Arnaldo "Boy" Mansilla</td>
            </tr>

            <tr>
              <td>Bible Study</td>
              <td>Noel Papa</td>
              <td>Vice Chair and Property Maintenance</td>
              <td>Lance Pabon</td>
            </tr>

            <tr>
              <td>Prayer</td>
              <td>Sol Quizan</td>
              <td>Risk Management and Security</td>
              <td>Norben Ramos</td>
            </tr>

            <tr>
              <td>Worship and Ordinance</td>
              <td>Sydney Solis</td>
              <td>Supplies</td>
              <td>Carol Pragados</td>
            </tr>

             <tr>
              <td>Care and Visitation</td>
              <td>Stanley Pido</td>
              <td>Rentals</td>
              <td>Dan Francisco</td>
            </tr>

            <tr>
              <td>Membership and Hospitality</td>
              <td>Rey Dohello</td>
              <td>Events</td>
              <td>Annie Griffith</td>
            </tr>
          </table> 
          
          <table>
            <tr>
              <th colspan="4">2019 Officers</th><!--table header-->
            </tr>

            <tr class="board">
              <th colspan="2">Ministry Team</th>
              <th colspan="2">Youth</th>
            </tr>
          
           <!-- Table 2 -->
           <tr>
              <td>Council Chairwoman</td>
              <td>Mary Lorraine A. Hernaez</td>
              <td>President</td>
              <td>Julius Uson</td>
            </tr>

             <tr>
              <td>Council Secretary</td>
              <td>Daisy Go-Gasgonia</td>
              <td>President</td>
              <td>Jan Chua</td>
            </tr>

            <tr>
              <td>Finance/ Treasurer</td>
              <td>Dulce Corazon Sanchez</td>
              <td>Secretary</td>
              <td>Dan Amelo</td>
            </tr>
            
            <tr>
              <td>Christian Education Director</td>
              <td>Thamie Tan</td>
              <td>Treasurer</td>
              <td>Cassidy Aguila</td>
            </tr>
            
            <tr>
              <td>Finance/ Treasurer</td>
              <td>Dulce Corazon Sanchez</td>
              <td></td>
              <td></td>
            </tr>
            
            <tr>
              <td>Sunday School Superintendent</td>
              <td>Kristien Sela</td>
              <td></td>
              <td></td>
            </tr>
            
             <tr>
              <td>Events</td>
              <td>Annie Griffith</td>
              <td></td>
              <td></td>
            </tr>
            
            <tr>
              <td>Men's President</td>
              <td>Lance Pabon</td>
              <td></td>
              <td></td>
            </tr>
            
            <tr>
              <td>Women's President</td>
              <td>Rebecca Werner</td>
              <td></td>
              <td></td>
            </tr>
            
            
         
        </table>  
      </div><!--wrapper close-->
    </section>

@endsection
   
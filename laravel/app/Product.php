<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    Public static function getAllProducts()
    {
        //return self::all();
        return self::latest()->get();
    }

    Public static function getOneProduct($id)
    {
        return self:: where('id', $id)->firstorfail();
    }
}

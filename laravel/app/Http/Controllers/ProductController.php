<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

class ProductController extends Controller
{
    Public function index()

    {   
        $slug = 'ecommerce';
       

        $products = Product::getAllProducts();
        //dd($products);
        return view('ecommerce',compact('slug', 'products'));
    }


    public function show($id)
    {
        $slug = 'ecommerce_details';
        $product = Product::getOneProduct($id);
        return view('ecommerce_details', compact('slug','product'));
    }

}

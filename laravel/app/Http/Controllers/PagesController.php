<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        $slug = 'home';
        $title = 'FEC Main Page';
        return view('home', compact('slug', 'title'));
    }

    public function about()
    {
        $slug = 'about';
        $title = 'FEC About Page';
        return view('about', compact('slug', 'title'));
    }

    public function activities()
    {
        $slug = 'activities';
        $title = 'FEC Activities Page';
        return view('activities', compact('slug', 'title'));
    }

    public function ministry()
    {
        $slug = 'ministry';
        $title = 'FEC Ministry Page';
        return view('ministry', compact('slug', 'title'));
    }

    

    public function address()
    {
        $slug = 'address';
        $title = 'FEC Address Page';
        return view('address', compact('slug', 'title'));
    }
}

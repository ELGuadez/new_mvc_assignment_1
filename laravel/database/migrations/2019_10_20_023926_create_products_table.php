<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('category', 255);
            $table->string('description', 2000);
            $table->decimal('cost', 7, 2);
            $table->decimal('price', 7, 2);
            $table->integer('quantity_delivered');
            $table->string('supplier', 255);
            $table->integer('stocks_available');
            $table->integer('reorder_level');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('products')->insert([
        	'name' => 'Printed black pullovers, Large',
            'description' => 'Lorem ipsum dolor sit amet, consectetur 
                            adipiscing elit, sed do eiusmod tempor incididunt 
                            ut labore et dolore magna aliqua. Ut enim ad minim 
                            veniam, quis nostrud exercitation ullamco laboris nisi 
                            ut aliquip ex ea commodo consequat. Duis aute irure 
                            dolor in reprehenderit in voluptate velit esse cillum 
                            dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
                            cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.',
            'category' => 'Apparel',
            'cost' => 32.00,
            'price' => 52.00,
            'quantity_delivered' => 20,
            'supplier' => 'Manitoba Shirt Professionals',
            'stocks_available' => 20,
            'reorder_level' => 2,
        	'image' => '288_black_pullovers.jpg',
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
        	'name' => 'Leggings for women, PH flag prints, small',
            'description' => 'Quam elementum pulvinar etiam non quam 
                            lacus suspendisse faucibus. Volutpat maecenas 
                            volutpat blandit aliquam etiam erat velit scelerisque. 
                            Arcu vitae elementum curabitur vitae nunc sed velit 
                            dignissim. Urna id volutpat lacus laoreet. Eu ultrices 
                            vitae auctor eu augue ut lectus. Et malesuada fames ac 
                            turpis egestas sed tempus urna. Cursus eget nunc scelerisque 
                            viverra mauris in. Arcu non sodales neque sodales ut etiam sit. 
                            Malesuada proin libero nunc consequat interdum varius. Condimentum 
                            mattis pellentesque id nibh tortor id aliquet lectus proin. 
                            Vestibulum morbi blandit cursus risus at. Tortor posuere ac 
                            ut consequat semper viverra nam libero. Aliquam sem fringilla 
                            ut morbi tincidunt augue interdum velit euismod. Non consectetur 
                            a erat nam at lectus urna. Vitae turpis massa sed elementum tempus 
                            egestas sed.',
            'category' => 'Apparel',
            'cost' => 18.00,
            'price' => 33.00,
            'quantity_delivered' => 20,
            'supplier' => 'WT Apparels',
            'stocks_available' => 20,
            'reorder_level' => 2,
            'image' => 'phil_leggings288.jpg',
            'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);

         DB::table('products')->insert([
        	'name' => 'Canada Flag inspired bag',
            'description' => 'Nisi quis eleifend quam adipiscing vitae proin. 
                            Quisque id diam vel quam elementum pulvinar etiam non quam. 
                            Quam quisque id diam vel quam. Lorem sed risus ultricies tristique 
                            nulla aliquet enim tortor at. Faucibus ornare suspendisse sed nisi 
                            lacus sed viverra tellus. Phasellus vestibulum lorem sed risus 
                            ultricies tristique nulla. Viverra orci sagittis eu volutpat odio. 
                            Montes nascetur ridiculus mus mauris vitae ultricies leo. In est ante 
                            in nibh mauris cursus mattis molestie a. Sed vulputate mi sit amet mauris. 
                            Suscipit tellus mauris a diam. Amet cursus sit amet dictum sit amet
                            justo donec. Morbi tristique senectus et netus et malesuada.',
            'category' => 'Accessories',
            'cost' => 50.00,
            'price' => 70.00,
            'quantity_delivered' => 20,
            'supplier' => 'Canadian Baggers',
            'stocks_available' => 20,
            'reorder_level' => 2,
        	'image' => 'canada_bag288.jpg',
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
        	'name' => 'Green white cap, God recycles',
            'description' => 'Vulputate dignissim suspendisse in est ante in nibh mauris. 
                            Facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat. Facilisis 
                            gravida neque convallis a cras semper. In massa tempor nec feugiat nisl pretium. Commodo 
                            ullamcorper a lacus vestibulum sed arcu non. Ullamcorper dignissim cras tincidunt 
                            lobortis feugiat vivamus at augue. Quam id leo in vitae turpis. Facilisis gravida 
                            neque convallis a cras. Est sit amet facilisis magna etiam tempor. Accumsan tortor 
                            posuere ac ut consequat. Nisl rhoncus mattis rhoncus urna neque viverra. Volutpat 
                            odio facilisis mauris sit amet. Tortor vitae purus faucibus ornare suspendisse sed nisi. 
                            Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Ipsum a arcu cursus 
                            vitae congue mauris rhoncus aenean. Leo duis ut diam quam nulla porttitor. Arcu bibendum 
                            at varius vel',
            'category' => 'Accessories',
            'cost' => 18.00,
            'price' => 30.00,
            'quantity_delivered' => 20,
            'supplier' => 'Sportchek Canada',
            'stocks_available' => 20,
            'reorder_level' => 2,
        	'image' => 'god_recycles_cap288.jpg',
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
        	'name' => 'Evangelical Bible',
            'description' => 'Quis eleifend quam adipiscing vitae proin. Nullam 
                            vehicula ipsum a arcu cursus. Bibendum est ultricies integer quis 
                            auctor elit. Nunc vel risus commodo viverra. Id interdum velit laoreet
                            id donec ultrices tincidunt arcu. Vel eros donec ac odio tempor orci 
                            dapibus ultrices in. Vestibulum sed arcu non odio. Pulvinar neque laoreet 
                            suspendisse interdum consectetur libero id faucibus nisl. Lobortis feugiat 
                            vivamus at augue eget arcu dictum varius duis. Sed sed risus pretium quam 
                            vulputate dignissim suspendisse. Ut morbi tincidunt augue interdum 
                            velit euismod in. Nulla pellentesque dignissim enim sit amet venenatis urna 
                            cursus. In aliquam sem fringilla ut morbi tincidunt augue interdum velit. Dictumst 
                            quisque sagittis purus sit amet volutpat. A cras semper auctor neque vitae tempus quam 
                            pellentesque nec. Varius sit amet mattis vulputate enim nulla aliquet porttitor lacus. 
                            Lectus magna fringilla urna porttitor rhoncus dolor purus.',
            'category' => 'Books',
            'cost' => 10.00,
            'price' => 17.00,
            'supplier' => 'Canada Bible Society',
            'stocks_available' => 20,
            'reorder_level' => 2,
            'quantity_delivered' => 20,
        	'image' => 'evagelical_bible288.jpg',
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);

        DB::table('products')->insert([
            'name' => 'When Christ came calling',
            'description' => 'Lorem ipsum dolor sit amet, consectetur 
                            adipiscing elit, sed do eiusmod tempor incididunt ut labore 
                            et dolore magna aliqua. Pellentesque nec nam aliquam 
                            sem et. Sagittis vitae et leo duis ut diam quam. Ullamcorper 
                            velit sed ullamcorper morbi tincidunt ornare massa. Nisl 
                            condimentum id venenatis a condimentum vitae sapien pellentesque 
                            habitant. Congue mauris rhoncus aenean vel. Felis eget nunc 
                            lobortis mattis. Pulvinar etiam non quam lacus suspendisse f
                            aucibus interdum posuere lorem. Quis hendrerit dolor magna eget 
                            est lorem ipsum dolor sit. Ultricies mi quis hendrerit dolor. Scelerisque
                            eleifend donec pretium vulputate sapien nec sagittis. 
                            Sollicitudin ac orci phasellus egestas.',
            'category' => 'Books',
            'cost' => 15.00,
            'price' => 25.00,
            'supplier' => 'Stephen George Publishing House',
            'stocks_available' => 15,
            'reorder_level' => 2,
            'quantity_delivered' => 20,
            'image' => 'when_christ_came_calling288.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
            'name' => 'Fitbit Versa Smartwatch',
            'description' => 'Quam id leo in vitae turpis. Pharetra 
                            et ultrices neque ornare. At consectetur lorem donec 
                            massa sapien faucibus. Sodales ut eu sem integer. 
                            Laoreet suspendisse interdum consectetur libero id 
                            faucibus nisl. Nisl tincidunt eget nullam 
                            non nisi est sit amet facilisis. Risus quis 
                            varius quam quisque id diam vel quam. Purus non 
                            enim praesent elementum facilisis leo. Suspendisse 
                            potenti nullam ac tortor. Nunc sed augue lacus 
                            viverra vitae congue eu consequat ac. Sed vulputate 
                            odio ut enim blandit volutpat maecenas volutpat blandit. 
                            Egestas purus viverra accumsan in nisl nisi. Sit amet 
                            venenatis urna cursus eget nunc.',
            'category' => 'Electronics',
            'cost' => 180.00,
            'price' => 250.00,
            'supplier' => 'Asia-Canada Electronics',
            'stocks_available' => 12,
            'reorder_level' => 2,
            'quantity_delivered' => 20,
            'image' => 'fitbit_versa288.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
        DB::table('products')->insert([
            'name' => 'Wired headphones',
            'description' => 'Sit amet consectetur adipiscing 
                            elit ut aliquam. Purus viverra accumsan in nisl 
                            nisi scelerisque eu ultrices. Vitae congue eu 
                            consequat ac felis donec et odio. Aliquam etiam erat 
                            velit scelerisque in dictum. Aliquam malesuada 
                            bibendum arcu vitae. Tincidunt dui ut ornare lectus 
                            sit amet. Lacinia at quis risus sed vulputate odio ut enim. 
                            Curabitur gravida arcu ac tortor dignissim convallis aenean 
                            et tortor. Tincidunt vitae semper quis lectus nulla at volutpat. 
                            Integer eget aliquet nibh praesent tristique magna sit amet purus.
                            Lobortis mattis aliquam faucibus purus in. Diam phasellus vestibulum 
                            lorem sed risus ultricies tristique. Et odio pellentesque diam 
                            volutpat commodo sed egestas egestas. Varius morbi enim nunc 
                            faucibus. Bibendum ut tristique et egestas quis ipsum suspendisse 
                            ultrices. Venenatis tellus in metus vulputate eu.',
            'category' => 'Electronics',
            'cost' => 16.00,
            'price' => 26.00,
            'supplier' => 'Dolby Digital Electronics',
            'stocks_available' => 10,
            'reorder_level' => 2,
            'quantity_delivered' => 25,
            'image' => '288_christian_headphones.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
        DB::table('products')->insert([
            'name' => 'PH flag coffee mug',
            'description' => 'Lorem ipsum dolor sit amet, 
                            consectetur adipiscing elit, sed do eiusmod 
                            tempor incididunt ut labore et dolore magna aliqua. 
                            Sed velit dignissim sodales ut eu sem integer vitae justo. 
                            Nam libero justo laoreet sit amet. Vitae sapien pellentesque 
                            habitant morbi tristique senectus. Pellentesque 
                            habitant morbi tristique senectus et netus et malesuada. 
                            Eu sem integer vitae justo. Tellus at urna condimentum mattis 
                            pellentesque id. Id aliquet lectus proin nibh nisl condimentum. 
                            Augue ut lectus arcu bibendum at varius vel. Turpis massa tincidunt dui 
                            ut ornare. Pharetra massa massa ultricies mi quis. Egestas sed sed 
                            risus pretium quam vulputate. Elementum eu facilisis sed odio morbi 
                            quis. Massa eget egestas purus viverra accumsan in. Eu tincidunt 
                            tortor aliquam nulla. Ante metus dictum at tempor commodo ullamcorper 
                            a lacus vestibulum. Etiam non quam lacus suspendisse faucibus interdum 
                            posuere lorem.',
            'category' => 'Household items',
            'cost' => 5.00,
            'price' => 9.00,
            'supplier' => 'Dolby Digital Electronics',
            'stocks_available' => 10,
            'reorder_level' => 2,
            'quantity_delivered' => 20,
            'image' => '288_ph_flag_mug.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
            'name' => '23rd Psalm Cotton Tapestry Throw Blanket',
            'description' => 'In nibh mauris cursus mattis 
                            molestie a iaculis. Etiam tempor orci eu lobortis elementum nibh 
                            tellus. Commodo ullamcorper a lacus vestibulum. Sed 
                            risus pretium quam vulputate dignissim suspendisse in. Ac 
                            placerat vestibulum lectus mauris ultrices eros in cursus. Iaculis 
                            at erat pellentesque adipiscing. Elementum integer enim 
                            neque volutpat. Gravida quis blandit turpis cursus in hac 
                            habitasse. Vitae suscipit tellus mauris a. Ipsum suspendisse 
                            ultrices gravida dictum fusce ut placerat orci. Venenatis urna 
                            cursus eget nunc. Sit amet est placerat in. Vulputate sapien nec 
                            sagittis aliquam malesuada bibendum arcu vitae. Turpis cursus in hac 
                            habitasse platea dictumst quisque sagittis purus.',
            'category' => 'Household items',
            'cost' => 50.00,
            'price' => 60.00,
            'supplier' => 'Mariposa Textiles',
            'stocks_available' => 18,
            'reorder_level' => 2,
            'quantity_delivered' => 20,
            'image' => 'psal_23_blanket288.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
